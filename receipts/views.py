from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryFrom, AccountForm

@login_required
def show_receipt(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "show_receipt": receipts,
    }
    return render(request, "receipts/list.html", context)
# Create your views here.

#Protect the list view for the Receipt model
# so that only a person who has logged in can access it.
# Change the queryset of the view to filter the Receipt objects
# where purchaser equals the logged in user

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipts = form.save(False)
            receipts.purchaser = request.user
            receipts.save()
            return redirect("home")

    else:
        # create an instance of the django model from class
        form = ReceiptForm()

    context = {
        "form": form,
    }
    #render the html template with the form
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": categories,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {
        "account_list": accounts
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryFrom(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")

    else:
        # create an instance of the django model from class
        form = ExpenseCategoryFrom()

    context = {
        "form": form,
    }
    #render the html template with the form
    return render(request, "receipts/create_categories.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")

    else:
        # create an instance of the django model from class
        form = AccountForm()

    context = {
        "form": form,
    }
    #render the html template with the form
    return render(request, "receipts/create_accounts.html", context)
